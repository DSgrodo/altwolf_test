using UnityEngine;

[CreateAssetMenu]
public class SoundsNamesContainer : ScriptableObject
{
    public readonly string CharacterCollision = "CharacterCollision";
    public readonly string Win = "Win";
    public readonly string Lost = "Lost";
    public readonly string ButtonPressed = "ButtonPressed";
}
