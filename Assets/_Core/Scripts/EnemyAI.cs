using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    [SerializeField] private Transform _player;
    [SerializeField] private Transform _arena;
    [SerializeField] private float _limitDistanceToCenter = 5f;
    [SerializeField] private float _reactionAngle = 5f;
    private Vector2 _center;
    private Vector2 _playerPosition;
    private Vector2 _selfDirection;
    private Vector2 _directionToCenter;
    private Vector2 _directionToPlayer;
    private CircleCharacterController _circleCharacterController;

    void Start()
    {
        InitCharacterController();
        InitCenter();
    }

    void Update()
    {
        UpdatePlayerPositionInfo();
        UpdateSelfDirectionInfo();
        UpdateDirectionToPlayerInfo();
        UpdateDirectionToCenterInfo();
        CheckIfNeedToMove();
    }

    private void CheckIfNeedToMove()
    {
        if (IsLookingAtPlayer() || (IsLookingAtCenter() && IsFarFromCenter()))
        {
            _circleCharacterController.needToMove = true;
        }
    }

    private bool IsFarFromCenter()
    {
        return Vector2.Distance(transform.position, _center) > _limitDistanceToCenter;
    }

    private bool IsLookingAtCenter()
    {
        return Vector2.Angle(_selfDirection, _directionToCenter) < _reactionAngle;
    }

    private bool IsLookingAtPlayer()
    {
        return Vector2.Angle(_selfDirection, _directionToPlayer) < _reactionAngle;
    }

    private void InitCenter()
    {
        _center = _arena.position;
    }

    private void InitCharacterController()
    {
        _circleCharacterController = GetComponent<CircleCharacterController>();
    }

    private void UpdateDirectionToCenterInfo()
    {
        _directionToCenter = (_center - (Vector2)transform.position).normalized;
    }

    private void UpdateDirectionToPlayerInfo()
    {
        _directionToPlayer = (_playerPosition - (Vector2)transform.position).normalized;
    }

    private void UpdateSelfDirectionInfo()
    {
        _selfDirection = transform.up;
    }

    private void UpdatePlayerPositionInfo()
    {
        _playerPosition = _player.position;
    }
}
