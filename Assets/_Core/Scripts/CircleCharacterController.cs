using UnityEngine;
using DG.Tweening;

public class CircleCharacterController : MonoBehaviour
{
    [SerializeField] private SoundsNamesContainer _sounds;
    [SerializeField] private TagsContainer _tags;
    [SerializeField] private float _fullRotationTime = 5f;
    [SerializeField] private float _forceModifier = 1f;
    [SerializeField] private bool _isRotatingClockwise = true;
    [HideInInspector] public bool needToMove = false;
    private Rigidbody2D _rigidbody;
    private int _rotationDirectionModifier = 1;

    void Start()
    {
        InitRigidbody();
        SetRotationDirectionModifier();
        StartConstantRotation();
    }

    private void FixedUpdate()
    {
        ChekIfNeedToMove();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == _tags.Enemy)
        {
            AudioManager.Instance.Play(_sounds.CharacterCollision);
        }
    }

    private void SetRotationDirectionModifier()
    {
        if (_isRotatingClockwise == true)
        {
            _rotationDirectionModifier = -1;
        }
        else
        {
            _rotationDirectionModifier = 1;
        }
    }

    private void ChekIfNeedToMove()
    {
        if (needToMove)
        {
            needToMove = false;
            Move();
        }
    }

    private void InitRigidbody()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Move()
    {
        _rigidbody.AddRelativeForce(Vector2.up * _forceModifier, ForceMode2D.Force);
    }

    private void StartConstantRotation()
    {
        transform.DORotate(new Vector3(0f, 0f, 360f) * _rotationDirectionModifier, _fullRotationTime, RotateMode.FastBeyond360)
            .SetEase(Ease.Linear)
            .SetLoops(-1, LoopType.Incremental);
    }
}
