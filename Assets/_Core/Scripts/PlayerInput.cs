using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    private CircleCharacterController _circleCharacterController;
    [SerializeField] private SoundsNamesContainer _sounds;

    void Start()
    {
        InitCharacterController();
    }

    void Update()
    {
        ChekForInput();
    }

    private void ChekForInput()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            SetMoveMarkerOn();
        }
    }

    public void SetMoveMarkerOn()
    {
        _circleCharacterController.needToMove = true;
        AudioManager.Instance.Play(_sounds.ButtonPressed);
    }

    private void InitCharacterController()
    {
        _circleCharacterController = GetComponent<CircleCharacterController>();
    }
}
