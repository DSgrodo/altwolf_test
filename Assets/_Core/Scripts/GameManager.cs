using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject _winUI;
    [SerializeField] private GameObject _looseUI;
    [SerializeField] private SoundsNamesContainer _sounds;

    void Start()
    {
        SubscribeToEvents();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            QuitGame();
        }
    }

    private void OnDestroy()
    {
        UnsubscribeFromEvents();
    }


    private void SubscribeToEvents()
    {
        GameEvents.Instance.OnEnemyOutOfBounds += OnEnemyOutOfBoundsHandler;
        GameEvents.Instance.OnPlayerOutOfBounds += OnPlayerOutOfBoundsHandler;
    }

    private void UnsubscribeFromEvents()
    {
        GameEvents.Instance.OnEnemyOutOfBounds -= OnEnemyOutOfBoundsHandler;
        GameEvents.Instance.OnPlayerOutOfBounds -= OnPlayerOutOfBoundsHandler;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        Time.timeScale = 1f;
    }

    public void QuitGame()
    {
        Application.Quit();
    }


    private void OnPlayerOutOfBoundsHandler()
    {
        _looseUI.SetActive(true);
        AudioManager.Instance.Play(_sounds.Lost);
        Time.timeScale = 0f;
    }

    private void OnEnemyOutOfBoundsHandler()
    {
        _winUI.SetActive(true);
        AudioManager.Instance.Play(_sounds.Win);
        Time.timeScale = 0f;
    }
}
