using System;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
    public static GameEvents Instance { get; private set; }

    public event Action OnEnemyOutOfBounds;
    public event Action OnPlayerOutOfBounds;

    private void Awake()
    {
        InitSingleton();
    }

    private void InitSingleton()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    public void EnemyOutOfBounds()
    {
        OnEnemyOutOfBounds?.Invoke();
    }

    public void PlayerOutOfBounds()
    {
        OnPlayerOutOfBounds?.Invoke();
    }

}
