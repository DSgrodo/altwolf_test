using UnityEngine;

public class CirclesExitTrigger : MonoBehaviour
{
    [SerializeField] private TagsContainer _tags;

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == _tags.Player)
        {
            GameEvents.Instance.PlayerOutOfBounds();
        }

        if (other.tag == _tags.Enemy)
        {
            GameEvents.Instance.EnemyOutOfBounds();
        }
    }
}
