using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class TagsContainer : ScriptableObject
{
    public readonly string Player = "Player";
    public readonly string Enemy = "Enemy";
}
